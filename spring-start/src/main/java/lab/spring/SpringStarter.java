package lab.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringStarter {

    public static void main(String[] args) {
        System.out.println("SpringStarter.main");

        ApplicationContext context =
                new AnnotationConfigApplicationContext(Config.class);
           //     new ClassPathXmlApplicationContext("classpath:applicationContext.xml");

        Box box = context.getBean(Box.class);
        System.out.println("[" + box.describeContent() + "]");

        Element c = (Element) context.getBean("candies");
        System.out.println("candies:" + c.getDescription());
    }

}
