package lab.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

@Component("candies")
@SweetsQualifier
public class Candies implements Element {

    @Resource
    private List<String> descriptions;

    public Candies(){
        System.out.println("candies constructor");
    }

    public void setDescriptions(List<String> descriptions) {
        this.descriptions = descriptions;
    }

    @Override
    public String getDescription() {
        return descriptions.toString();
    }
}
