package lab.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Primary
@PropertySource({"lab.properties"})
public class Meal implements Element{

    //@Autowired
    @Value("${meal.name}") // SpEL
    private String description;

    //@Autowired
    //Environment env;

    @Override
    public String getDescription() {
        return description;
        //return env.getProperty("meal.name");
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
