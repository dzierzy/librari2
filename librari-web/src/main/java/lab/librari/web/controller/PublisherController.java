package lab.librari.web.controller;

import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@Controller
@Adviced
public class PublisherController {

    Logger logger = Logger.getLogger(PublisherController.class.getName());

    @Autowired
    BrowsingService bs;

    //@RequestMapping(value = "/publishers", method = RequestMethod.GET)
    @GetMapping("/publishers")
    public String getPublishers(Model model){
        logger.info("about to fetch publishers list");

        List<Publisher> publishers = bs.getPublishers();

        model.addAttribute("publishers", publishers);

        return "publishers";
    }

    @GetMapping("/publishersData")
    public @ResponseBody List<Publisher> getPublishersData(){
        return bs.getPublishers();
    }




}
