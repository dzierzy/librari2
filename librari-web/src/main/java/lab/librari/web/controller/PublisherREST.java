package lab.librari.web.controller;

import lab.librari.model.Book;
import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class PublisherREST {


    @Autowired
    BrowsingService bs;

    @GetMapping("/publishers")
    public List<Publisher> getPublishersData(){
        return bs.getPublishers();
    }

    @GetMapping("/publishers/{id}")
    public Publisher getPublisher(@PathVariable("id") long publisherId){
        return bs.getPublisher(publisherId);
    }

    @GetMapping("/publishers/{id}/books")
    public List<Book> getBooks(@PathVariable("id") long publisherId){
        return bs.getBooksForPublisher(publisherId);
    }

    @PostMapping("/publishers/{id}/books")
    public ResponseEntity<Book> addBook(
            @PathVariable("id") long publisherId,
            @RequestBody Book book ){

        book = bs.addBook(bs.getPublisher(publisherId), book);

        return ResponseEntity.status(HttpStatus.CREATED).body(book);
        //return new ResponseEntity(book, HttpStatus.CREATED);

    }

}
