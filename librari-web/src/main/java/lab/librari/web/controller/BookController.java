package lab.librari.web.controller;

import lab.librari.model.Book;
import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@Controller
@Adviced
@RequestMapping("/books")
@SessionAttributes("publisher")
public class BookController {

    Logger logger = Logger.getLogger(BookController.class.getName());

    @Autowired
    BrowsingService bs;

    @Autowired
    BookValidator validator;

    @InitBinder("bookForm")
    void initBinding(WebDataBinder binder){
        binder.setValidator(validator);
    }

    //@RequestMapping(value = "/books", method = RequestMethod.GET)
    @GetMapping
    public String getBooks(Model model, @RequestParam("publisherId") long publisherId){
        logger.info("about to fetch books of publishers " + publisherId);

        List<Book> books = bs.getBooksForPublisher(publisherId);

        model.addAttribute("books", books);
        model.addAttribute("publisherId", publisherId);

        return "books";
    }

    @GetMapping("/add")
    public String addBookPrepare(Model model, @RequestParam("publisherId") long publisherId){
        logger.info("preparing add book action for publisher " + publisherId);

        model.addAttribute("bookForm", new Book());
        model.addAttribute("publisher", bs.getPublisher(publisherId));

        return "addBook";

    }


    @PostMapping("/add")
    public String addBook(
            @ModelAttribute("bookForm") @Validated Book book,
            BindingResult br,
            @SessionAttribute("publisher") Publisher p){

        logger.info("adding book " + book + " for publisher " + p.getId() );

        if(br.hasErrors()){
            return "addBook";
        }

        bs.addBook(p, book);

        return "redirect:/books?publisherId=" + p.getId();

    }





}
