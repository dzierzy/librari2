package lab.librari.web.servlet;

import lab.librari.service.api.BrowsingService;
import lab.librari.service.impl.BrowsingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

//@WebServlet("/books")
public class BookServlet extends HttpServlet {

    Logger logger = Logger.getLogger(BookServlet.class.getName());

    BrowsingService bs = new BrowsingServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        long id = Long.parseLong(req.getParameter("publisherId"));
        logger.info("about to fetch books of publisher " + id);

        req.setAttribute("books", bs.getBooksForPublisher(id));

        req.getRequestDispatcher("/WEB-INF/jsp/books.jsp").forward(req,resp);
    }
}
