package lab.librari.web.servlet;

import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import lab.librari.service.impl.BrowsingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

//@WebServlet("/publishers")
public class PublisherServlet extends HttpServlet {

    Logger logger = Logger.getLogger(PublisherServlet.class.getName());

    private BrowsingService bs = new BrowsingServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("about to fetch publishers list");

        List<Publisher> publishers = bs.getPublishers();

        req.setAttribute("publishers", publishers);
        //req.setAttribute("slogan", "Because you read.");

        req.getRequestDispatcher("/WEB-INF/jsp/publishers.jsp").forward(req,resp);

        /*StringBuilder sb = new StringBuilder();
        sb.append("<html><body>");
        sb.append("<table><tr><th>Name</th><th>Logo</th></tr>");

        for(Publisher p : publishers){
            sb.append(
            "<tr><td>" + p.getName() + "</td><td><img src='" + p.getLogoImage() +"'/></td></tr>" );
        }

        sb.append("</table></body></html>");

        resp.getWriter().write(sb.toString());*/

    }





}
