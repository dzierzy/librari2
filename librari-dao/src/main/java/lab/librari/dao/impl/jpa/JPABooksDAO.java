package lab.librari.dao.impl.jpa;

import lab.librari.dao.BooksDAO;
import lab.librari.model.Book;
import lab.librari.model.Publisher;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Primary
public class JPABooksDAO implements BooksDAO {

    @PersistenceContext(name = "librariUnit")
    EntityManager em;
            /*= Persistence.
            createEntityManagerFactory("librariUnit").
            createEntityManager();*/

    @Override
    public List<Publisher> getAllPublishers() {
        // JPQL -> HQL -> SQL
        return em.createQuery("select p from Publisher p").getResultList();
    }

    @Override
    public List<Publisher> getPublishersPage(int pageNumber, int pageSize) {
        return null;
    }

    @Override
    public Publisher getPublisherById(Long id) {
        return em.find(Publisher.class, id);
    }

    @Override
    public List<Book> getBooksByPublisher(Publisher p) {
        return em.
                createQuery("select b from Book b where b.publisher=:p").
                setParameter("p", p).
                getResultList();
    }

    @Override
    public Book getBookById(Long mId) {
        return em.find(Book.class, mId);
    }

    @Override
    public Publisher addPublisher(Publisher r) {
        return null;
    }

    @Override
    //@Transactional(propagation = Propagation.NEVER)
    public Book addBook(Book b) {
        em.persist(b);
        return b;
    }
}
