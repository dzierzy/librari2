package lab.librari.jsf;


import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

// POJO = Plain Old Java Object == JavaBean
@ManagedBean(name = "helloBean")
//@RequestScoped
@ApplicationScoped
public class HelloBean {

    private int count;

    public String getHelloMessage(){
        return "Hey Joe!";
    }

    public int getCount() {
        return ++count;
    }

}
