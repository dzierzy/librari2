package lab.librari.jsf;

import lab.librari.model.Book;
import lab.librari.model.Publisher;
import lab.librari.service.api.BrowsingService;
import lab.librari.service.impl.BrowsingServiceImpl;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ReferencedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

//@ManagedBean(name = "publishersBean")
public class PublishersBean {

    private BrowsingService bs = new BrowsingServiceImpl();

    private long publisherId;

    public List<Publisher> getPublishers(){
        return bs.getPublishers();
    }

    public List<Book> getBooks(){

        /*String idString = ((HttpServletRequest)FacesContext.
                getCurrentInstance().
                getExternalContext().
                getRequest()).getParameter("publisherId");*/
        return bs.getBooksForPublisher(publisherId);
    }

    public long getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(long publisherId) {
        this.publisherId = publisherId;
    }
}
